package CucumberTests;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.assertEquals;

/**
 * Created by Adam on 11/01/2021
 *
 * @author : Adam
 * @date : 11/01/2021
 * @projects : StringNameMatcher
 */
public class CalculatorStepsTEST {

    Integer x,y,total;
    Calculator cal = new Calculator();


    @Given("when provided  {int} and {int}")
    public void when_provided_and(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }

    @When("function is called")
    public void function_is_called() {
       total = cal.addition(this.x,this.y);
    }

    @Then("the result is {int}")
    public void the_result_is(Integer total) {
        assertEquals(total,this.total);
    }




}
